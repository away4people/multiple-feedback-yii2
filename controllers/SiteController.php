<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\MultipleFeedbackForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
            // проверяем, заполнял ли ранее форму клиент
        $model = MultipleFeedbackForm::find()->where(['client_id' => Yii::$app->user->identity->id])->one();

            // если нет, то рендерится/обрабатывается первый шаг формы
        if (!isset($model)) {
            $model = new MultipleFeedbackForm(['scenario' => MultipleFeedbackForm::SCENARIO_FIRST_STEP]);
            if ($model->load(Yii::$app->request->post()) && $model->stepSubmit()) {
                return $this->refresh();
            }
        }
        
            // проверяем, заполнял ли ранее второй шаг (путём определения заполненности поля address в БД
            // если нет, то рендерится/обрабатывается второй шаг формы
        else if (!isset($model->address)) {
            $model->scenario = MultipleFeedbackForm::SCENARIO_SECOND_STEP;
            if ($model->load(Yii::$app->request->post()) && $model->stepSubmit()) {
                return $this->refresh();
            }
        }
        
            // проверяем, заполнял ли ранее третий шаг (путём определения заполненности поля comment в БД
            // если нет, то рендерится/обрабатывается третий шаг формы
        else if (!isset($model->comment)) {
            $model->scenario = MultipleFeedbackForm::SCENARIO_THIRD_STEP;
            if ($model->load(Yii::$app->request->post()) && $model->stepSubmit()) {
                    return $this->refresh();
            }
        }
        
        
        return $this->render('index', [
                'model' => $model,
        ]);
    }
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }


}
