<?php

use yii\db\Migration;

/**
 * Handles the creation of table `multiplefeedbackform`.
 */
class m181110_182039_create_multiplefeedbackform_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('multiplefeedbackform', [
            'client_id' => $this->primaryKey(),
            'name' => $this->string(20)->notNull(), 
            'surname' => $this->string(20)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'address' => $this->string(100),
            'comment' => $this->text(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp(),
            'feedback_data_id' => $this->string(64),
        ]);
        
        $this->createIndex(
            'idx-post-phone',
            'multiplefeedbackform',
            'phone'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('multiplefeedbackform');
    }
}
