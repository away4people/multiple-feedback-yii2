<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * ContactForm is the model behind the contact form.
 */
class MultipleFeedbackForm extends ActiveRecord
{
    const SCENARIO_FIRST_STEP = 'first-step';
    const SCENARIO_SECOND_STEP = 'second-step';
    const SCENARIO_THIRD_STEP = 'third-step';
    const URL = 'http://test.vrgsoft.net/feedbacks';

    public static function tableName()
    {
        return '{{multiplefeedbackform}}';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'phone'], 'required', 'on' => self::SCENARIO_FIRST_STEP],
            ['phone', 'match', 'pattern' => '/\([0-9]{3}\)\s[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/', 'message' => 'format: (xxx) xxx-xx-xx'],
            [['address'], 'required', 'on' => self::SCENARIO_SECOND_STEP],
            [['comment'], 'required', 'on' => self::SCENARIO_THIRD_STEP],
        ];
    }
    
    public function scenarios()
    {
        return [
            self::SCENARIO_FIRST_STEP => ['name', 'surname', 'phone'],
            self::SCENARIO_SECOND_STEP => ['address'],
            self::SCENARIO_THIRD_STEP => ['comment'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        $this->updated_at = date('Y-m-d H:i:s');
        return parent::beforeSave($insert);
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function stepSubmit()
    {
        switch($this->scenario){
            case self::SCENARIO_FIRST_STEP : 
                $this->client_id = Yii::$app->user->identity->id;
                break;
            case self::SCENARIO_THIRD_STEP :
                $this->feedback_data_id = self::sendFeedback($this->client_id, $this->address, $this->comment);
                if ($this->feedback_data_id == false){
                    $this->addError('comment', 'Увы, удаленный ресурс не отвечает!');
                    return false;
                }
                break;
        }
        
        return $this->save();
    }
    
    public function sendFeedback($clientId, $address, $comment) {
        $ch = curl_init();      
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   
        curl_setopt($ch, CURLOPT_URL, self::URL);    
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 5); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=".$clientId."&address=".$address."&comment=".$comment.""); 
        $result = curl_exec($ch);  
        if (curl_getinfo( $ch, CURLINFO_HTTP_CODE ) !== 200){
            Yii::error(date('Y-m-d H:i:s')." | Error: wrong http-code from remote node. ");
            $result = false;
        }
        curl_close($ch);
        return $result;
    }
}
