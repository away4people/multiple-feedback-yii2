<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Multiple feedback form';
?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">

                <?php
                if($model->scenario != 'default'){
                    $form = ActiveForm::begin([
                                'id' => 'feedback-form',
                                'options' => ['class' => 'form-horizontal'],
                    ]);
                    switch($model->scenario){
                        case $model::SCENARIO_FIRST_STEP:
                            echo $form->field($model, 'name');
                            echo $form->field($model, 'surname');
                            echo $form->field($model, 'phone');
                            break;
                        case $model::SCENARIO_SECOND_STEP:
                            echo $form->field($model, 'address');
                            break;                        
                        case $model::SCENARIO_THIRD_STEP:
                            echo $form->field($model, 'comment');
                            break; 
                    }
                    
                ?>

                    <div class="form-group">
                        <div class="col-lg-offset-1 col-lg-11">
                            <?= Html::submitButton('Next step', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                <?php 
                    ActiveForm::end();
                }else{
                    echo $model->feedback_data_id;
                }
                
                ?>

            </div>
        </div>

    </div>
</div>
